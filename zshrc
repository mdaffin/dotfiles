export EDITOR='vim'

bindkey '\e[A' history-beginning-search-backward
bindkey '\e[B' history-beginning-search-forward

HISTSIZE=10000
SAVEHIST=10000
HISTFILE=$HOME/.local/share/zsh/history
mkdir -p $(dirname $HISTFILE)

export SSH_AUTH_SOCK="$XDG_RUNTIME_DIR/ssh-agent.socket"
export GOPATH=$HOME/projects/go
export PATH=$HOME/.cargo/bin:$GOPATH/bin:$HOME/bin:$PATH
